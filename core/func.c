/*
* This is the master of list of functions.
* All functions must be: "void foo();".
* And be defined in "foo.c".
*
* Please keep in alpha-num order.
*
* These functions may be wrappers for more complex functions.
*
* REMEMBER TO UPDATE LWMAPGEN_MAX_FUNC IN FUNC.H
*
* Copyright (C) 2003,2004 David Redick
* Released under the GNU General Public License (v2)
*/

#include "func.h"

/* is there a cleaner way of doing this? */
void big_quad();
void boxes();
void bubbles();
void burst();
void circles();
void circuit();
void hole();
void lines();
void maze();
void rand_box();
void rand_poly();
void rand_poly_cut();
void spirals();
void street();
void worms();


func_t func[LWMAPGEN_MAX_FUNC] =
{
     { "big_quad", "One big, solid quad that takes up most of the map.", big_quad },
     { "boxes", "A bunch of boxes of the same size.", boxes },
     { "bubbles", "Random bubbles.", bubbles },
     { "burst", "A firework burst.", burst },
     { "circles", "Random circles.", circles },
     { "circuit", "A map that looks like a circuit board.", circuit },
     { "hole", "Randomly shaped hole.", hole },
     { "lines", "A grid of random lines.", lines },
     { "maze", "A simple maze.", maze },
     { "rand_box", "Random boxes.", rand_box },
     { "rand_poly", "One big, solid, random polygon that takes up most of the map.", rand_poly },
     { "rand_poly_cut", "Do rand_poly then cut lines across it.", rand_poly_cut },
     { "spirals", "Makes crazy spirals.", spirals },
     { "street", "A map that looks like a bunch of streets.", street },
     { "worms", "Random little worms.", worms }
};
