/*
* Creates a maze using Prim's method.
*
* Copyright (C) 2004 David Redick
* Released under the GNU General Public License (v2)
*/

#include <stdio.h> /* for debugging */

#include "map.h"
#include "data_struct.h"


/* maze marks - the path that the 'turtle' makes to create the maze */
#define UNMARKED     0x00
#define UP           0x01
#define DOWN         0x02
#define LEFT         0x04
#define RIGHT        0x08
#define DEAD_END     0x10

/* maze directions */
#define NORTH     0
#define SOUTH     1
#define EAST      2
#define WEST      3

/* TESTING size */
#define MAZE_ROW_MAX     20
#define MAZE_COL_MAX     30


/* for debugging */
#ifdef LWMAPGEN_DEBUG
static void print_maze()
{
	/* hex print */
	for( r = 0; r < MAZE_ROW_MAX; r++ )
	{
     	for( c = 0; c < MAZE_COL_MAX; c++ )
          	printf(" 0x%x", maze_map[r][c]);
	     printf("\n");
	}

	/* top row line */
	for( c = 0; c < MAZE_COL_MAX; c++ )
     	printf("+---");
	printf("+\n");
	for( r = 0; r < MAZE_ROW_MAX; r++ )
	{
     	/* c == 0, always has wall to left */
	     printf("|");
     	for( c = 0; c < MAZE_COL_MAX; c++ )
	     {
     	     /* if maze path went RIGHT then don't draw right wall */
          	if( maze_map[r][c] & RIGHT )
               	/*    "+---+" */
	               printf("    ");
     	     else
          	/* if the cell to EAST went LEFT then don't draw right wall */
	          if( c < MAZE_COL_MAX-1 && maze_map[r][c+1] & LEFT )
     	          printf("    ");
          	else
               	printf("   |");
	     }

     	printf("\n");

	     for( c = 0; c < MAZE_COL_MAX; c++ )
     	{
          	/* if maze path went down then don't draw lower wall */
     	     if( maze_map[r][c] & DOWN )
	               /*      "   |" */
          	     printf("+   ");
	          else
     	     /* if cell to SOUTH went UP then don't draw lower wall */
          	if( r < MAZE_ROW_MAX-1 && maze_map[r+1][c] & UP )
               	printf("+   ");
	          else 
     	          printf("+---");
	     }
     	printf("+\n");
	}

return;
}
#endif


/*
* +---+---+
* | A |   |
* +---+---+
* | B |   |
* +---+---+
*
* we're given r,c of cell A.
* but the offset function just grabs the upper left corner pixel
* so to draw the lower wall we can just grab the pixel of the cell B
*/

static void draw_lower_wall( int r, int c )
{
     int x, y;

     offset(&x, &y, r+1, c);
     line(map.map, x, y, x+(int)map.sec_width, y, 0);
return;
}


/*
* +---+---+
* | A | B |
* +---+---+
* |   |   |
* +---+---+
*
* we're given r,c of cell A.
* but the offset function just grabs the upper left corner pixel
* so to draw the right wall we can just grab the pixel of the cell B
*/

static void draw_right_wall( int r, int c )
{
     int x, y;

     offset(&x, &y, r, c+1);
     line(map.map, x, y, x, y+(int)map.sec_height, 0);
return;
}


static int get_mark( int dir )
{
     int mark;


     switch(dir)
     {
          case NORTH: mark = UP;    break;
          case SOUTH: mark = DOWN;  break;
          case EAST:  mark = RIGHT; break;
          case WEST:  mark = LEFT;  break;

          default:
               fprintf(stderr, "error: unknown direction\n");
               mark = -99;
     }
return mark;
}


static void get_cell( int *tor, int *toc, int fromr, int fromc, int dir )
{
     switch(dir)
     {
          case NORTH:
               *tor = fromr - 1;
               *toc = fromc;
               break;

          case SOUTH:
               *tor = fromr + 1;
               *toc = fromc;
               break;

          case EAST:
               *tor = fromr;
               *toc = fromc + 1;
               break;

          case WEST:
               *tor = fromr;
               *toc = fromc - 1;
               break;

          default:
               fprintf(stderr, "error: unknown direction\n");
               *tor = -1;
               *toc = -1;
     }

     /* make sure we don't go off the map */
     if( *tor >= map.num_row || *tor < 0 ) *tor = -1;
     if( *toc >= map.num_col || *toc < 0 ) *toc = -1;

return;
}


/*
* NOTE: since the stack uses x, y instead of r, c
* I'm going to used r == x and c == y
*/


void maze()
{
     char start_dir, dir;
     int **maze_map;
     stack_node_t *top, *n;
     int r, c, tor, toc;
     int mark;


     maze_map = (int **)new_grid(map.num_row , map.num_col , sizeof(int));

     top = NULL;

     /* mark all cells as unmarked */
     for( r = 0; r < map.num_row; r++ )
          for( c = 0; c < map.num_col; c++ )
               maze_map[r][c] = UNMARKED;


     /* pick a random starting point */
     r = rand() % map.num_row;
     c = rand() % map.num_col;
printf("start: %d, %d\n", r, c);

     top = push_stack_node(top, r, c);

     while( top != NULL )
     {
          n = top;
          r = n->x;
          c = n->y;

          /* pick random direction that leads to an unmarked cell */
          mark = UNMARKED;
          dir = rand()%4;
          start_dir = dir;
          get_cell(&tor, &toc, r, c, dir);
          /* the -1 checks is to skip dirs that lead off the map */
          while( (tor == -1 || toc == -1) || maze_map[tor][toc] != UNMARKED )
          {
               dir++;
               if( dir >= 4 ) dir = 0;
               if( dir == start_dir )
               {
                    /* we've tried every dir so quit */
                    mark = DEAD_END;
                    break;
               }
               get_cell(&tor, &toc, r, c, dir);
          }

          /* if we can't go any where */
          if( mark == DEAD_END )
          {
               maze_map[r][c] |= DEAD_END;
               top = pop_stack_node(top, &n);
               free(n);
          }
          else
          {
               /* mark current cell as going that direction */
               mark = get_mark(dir);
               maze_map[r][c] |= mark;

               /* push new cell */
               top = push_stack_node(top, tor, toc);
          }

     } /* while stack not empty */



     /* draw the maze */
	for( r = 0; r < map.num_row; r++ )
	{
     	for( c = 0; c < map.num_col; c++ )
	     {
     	     /* if maze path went RIGHT then remove the right wall */
          	/* or if the cell to EAST went LEFT then remove the right wall */
               /* so if not A and not B then draw LEFT wall */
          	if( !(maze_map[r][c] & RIGHT)
               && !(c < map.num_col-1 && maze_map[r][c+1] & LEFT) )
               {
                    draw_right_wall(r, c);
               }


               if( !(maze_map[r][c] & DOWN)
               && !(r < map.num_row-1 && maze_map[r+1][c] & UP) )
               {
                    draw_lower_wall(r, c);
               }
	     }
	}

return;
}
