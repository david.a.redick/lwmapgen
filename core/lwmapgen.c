/*
* The basic wrapper/interface to the random map generator.
* 
* NOTE: I assume that all args are valid.
*
* size must be LWMAPGEN_MIN_MAP_SIZE to LWMAPGEN_MAX_MAP_SIZE-1 inclusive
* or LWMAPGEN_RAND_MAP_SIZE
*
* grid_size must be LWMAPGEN_MIN_GRID_SIZE to LWMAPGEN_MAX_GRID_SIZE-1 inclusive
* or LWMAPGEN_RAND_GRID_SIZE
*
* func_id must be LWMAPGEN_MIN_FUNC to LWMAPGEN_MAX_FUNC-1 inclusive
* or LWMAPGEN_RAND_FUNC
*
* Copyright (C) 2003,2004, David Redick
* Released under the GNU General Public License (v2)
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "lwmapgen.h"
#include "misc_math.h"

BITMAP * lwmapgen( int size, int grid_size, int func_id )
{
     /* get bitmap width, height */
     if( size == LWMAPGEN_RAND_MAP_SIZE )
          size = rand_num(LWMAPGEN_MIN_MAP_SIZE, LWMAPGEN_MAX_MAP_SIZE-1);
     map.width  = map_size[size][0];
     map.height = map_size[size][1];

printf("map.width=%d, map.height=%d\n", map.width, map.height);

     /* malloc space for the bitmap */
     map.map = create_bitmap_ex(8, map.width, map.height);


     /* get map grid size */
     if( grid_size == LWMAPGEN_RAND_GRID_SIZE )
          grid_size = rand_num(LWMAPGEN_MIN_GRID_SIZE, LWMAPGEN_MAX_GRID_SIZE-1);
     map.num_row = map_grid_size[grid_size][0];
     map.num_col = map_grid_size[grid_size][1];


     /* compute grid section width, height */
     map.sec_width  = (float)map.width  / (float)map.num_col;
     map.sec_height = (float)map.height / (float)map.num_row;


     /* set default color as black */
     map.color = 0;


     /* get generating function */
     if( func_id == LWMAPGEN_RAND_FUNC )
          func_id = rand_num(LWMAPGEN_MIN_FUNC, LWMAPGEN_MAX_FUNC-1);
     map.func_id = func_id;
     map.func = func[func_id].func;


     /* clear all the pixels in bitmap to white */
     clear_map();


/* debug prints */
printf("Generating random map using:\n\t%2d)  %s\t%s\n",
func_id, func[func_id].name, func[func_id].desc);
print_map();

     /* do the actual generating */
     (*map.func)();

/* debug prints */
print_map();

return map.map;
}
