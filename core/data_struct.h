/*
* Useful data structs.  Array, Matrix, List and Stack.
*  -- man, i wish C04 would be created with these
*
* Copyright (C) 2003,2004 David Redick
* Released under the GNU General Public License (v2)
*/

#ifndef _LWMAPGEN_DATA_STRUCT_H
#define _LWMAPGEN_DATA_STRUCT_H

/* size == the size of the elements in the array/grid */

void * new_array( int length, int size );
void delete_array( void *array );

void ** new_grid( int r, int c, int size );
void delete_grid( void **grid, int r );

/******************************************************************************/

typedef struct list_node
{
     int x, y;
     struct list_node *prev;
     struct list_node *next;
} list_node_t;


list_node_t * new_list_node();

/* returns new head */
list_node_t * add_list_node( list_node_t *head, int x, int y );

/******************************************************************************/

typedef list_node_t stack_node_t;


stack_node_t * new_stack_node();

/* returns new top */
stack_node_t * push_stack_node( stack_node_t *top, int x, int y );
stack_node_t * pop_stack_node( stack_node_t *top, stack_node_t **n );

#endif
