/*
* Useful data structs.  Array, Matrix, List and Stack.
*  -- man, i wish C04 would be created with these
*
* Copyright (C) 2003,2004 David Redick
* Released under the GNU General Public License (v2)
*/

#include <stdio.h>
#include <stdlib.h>

#include "data_struct.h"


/*****************************************************************************/

void * new_array( int length, int size )
{
     void *array;

     if( length == 0 || size == 0 )
     {
          fprintf(stderr, "can't create an array of 0 length. [%d](%d)\n", length, size);
          return NULL;
     }

     array = malloc(length*size);
     if( array == NULL )
     {
          fprintf(stderr, "fatal error: can't malloc space for array.\n");
          exit(EXIT_FAILURE);
     }

return array;
}

void delete_array( void *array )
{
     free(array);
return;
}

/*****************************************************************************/

void ** new_grid( int r, int c, int size )
{
     void **grid;
     int i;

     if( r == 0 || c == 0 || size == 0 )
     {
          fprintf(stderr, "can't create grid with 0 dimenson/size [%d][%d](%d).\n", r, c, size);
          return NULL;
     }

     grid = (void **) malloc( r * sizeof(void*) );
     if( grid == NULL )
     {
          fprintf(stderr, "fatal error: can't malloc space for grid rows.\n");
          exit(EXIT_FAILURE);
     }

     for( i = 0; i < r; i++ )
          grid[i] = (void *) new_array(c,size);

return grid;
}

void delete_grid( void **grid, int r )
{
     int i;

     if( r == 0 )
          fprintf(stderr, "can't free grid with 0 rows.\n");

     for( i = 0; i < r; i++ )
          delete_array(grid[i]);

     free(grid);
return;
}

/******************************************************************************/

list_node_t * new_list_node()
{
     list_node_t *n;

printf("sizeof(list_node_t) == %d\n", sizeof(list_node_t));
     n = malloc(sizeof(list_node_t));
     if( n == NULL )
     {
          fprintf(stderr, "error: out of mem. can't malloc list_node.\n");
          exit(EXIT_FAILURE);
     }

     n->x = 0;
     n->y = 0;
     n->next = NULL;
     n->prev = NULL;

return n;
}


list_node_t * add_list_node( list_node_t *head, int x, int y )
{
     if( head == NULL )
     {
printf("before new list node\n");
          head = new_list_node();
printf("after new list node\n");
          head->x = x;
          head->y = y;
          head->prev = head;
     }
     else
     {
          list_node_t *n;

printf("before new list node\n");
          n = new_list_node();
printf("after new list node\n");
          n->x = x;
          n->y = y;

          head->prev->next = n;
          n->prev = head->prev;
          head->prev = n;
     }

return head;
}

/******************************************************************************/

stack_node_t * new_stack_node()
{
     return (stack_node_t *) new_list_node();
}


stack_node_t * push_stack_node( stack_node_t *top, int x, int y )
{
     if( top == NULL )
     {
          top = new_stack_node();
          top->x = x;
          top->y = y;
     }
     else
     {
          stack_node_t *n;

          n = new_stack_node();
          n->x = x;
          n->y = y;
          n->next = top;
          top = n;
     }

return top;
}


stack_node_t * pop_stack_node( stack_node_t *top, stack_node_t **n )
{
     if( top == NULL )
          *n = NULL;
     else
     {
          *n = top;
          top = top->next;
     }
return top;
}
