/*
* Burst - A firework burst.
*
* A bunch of random circles with lines connecting to the center.
*
* Copyright (C) 2004 David Redick
* Released under the GNU General Public License (v2)
*/

#include <stdio.h>
#include <stdlib.h>

#include "map.h"
#include "data_struct.h"


static void cut_line( int fromx, int fromy, int tox, int toy, int line_size )
{
     int diff;
     int vert[8];


     diff = fromx-tox;
     diff = (diff < 0 ? diff*-1 : diff);


     /* up/down */
     if( diff <= line_size+3 )
     {
          vert[0] = fromx - line_size;
          vert[1] = fromy;
          vert[2] = fromx + line_size;
          vert[3] = fromy;

          vert[4] = tox + line_size;
          vert[5] = toy;
          vert[6] = tox - line_size;
          vert[7] = toy;
     }
     /* left/right */
     else
     {
          vert[0] = fromx;
          vert[1] = fromy + line_size;
          vert[2] = fromx;
          vert[3] = fromy - line_size;

          vert[4] = tox;
          vert[5] = toy - line_size;
          vert[6] = tox;
          vert[7] = toy + line_size;
     }

     polygon(map.map, 4, vert, 255);
return;
}


void burst()
{
     int x, y;
     int r, c;
     int centerr, centerc;
     int centerx, centery;
     int line_size, radius;
     char cut;
     list_node_t *head, *n, *temp;


     clear_invert_map();

     centerr = map.num_row/2;
     centerc = map.num_col/2;
     section_center(&centerx, &centery, centerr, centerc);

     radius = (map.sec_width > map.sec_height ? map.sec_width : map.sec_height)/4.0;
     radius = (radius == 0 ? 1 : radius );

     line_size = (map.sec_width > map.sec_height ? map.sec_width : map.sec_height)/4.0;
     line_size = (line_size == 0 ? 1 : line_size);

     head = NULL;

     for( r = 0; r < map.num_row; r++ )
     {
          for( c = 0; c < map.num_col; c++ )
          {
               cut = 0;

               /*
               * if top left, top center, top right
               * or center
               * or bottom left, bottom center, bottom right
               */
               if( (r == 0 && (c == 0 || c == centerc || c == map.num_col-1))
               || (r == centerr && c == centerc)
               || (r == map.num_row-1
               && (c == 0 || c == centerc || c == map.num_col-1)))
                    cut = 1;
               else if( rand()%3 == 0 )
                    cut = 1;
               else
                    cut = 0;

               if( cut == 1 )
               {
                    section_center(&x, &y, r, c);
                    circlefill(map.map, x, y, radius, 255);
printf("cutting(r,c): %d, %d\n", r, c);
                    head = add_list_node(head, x, y);
printf("after adding list node\n");
               }
          }
     }


     for( n = head; n != NULL; n = temp )
     {
          cut_line(n->x, n->y, centerx, centery, line_size);
          temp = n->next;
          free(n);
printf("freeing node...\n");
     }

     /* TODO: do i need this: cut outline around map... */

return;
}
