/*
* Various math functions.
*
* Copyright (C) 2003,2004 David Redick
* Released under the GNU General Public License (v2)
*/

#include <stdio.h>
#include <stdlib.h>

#include "misc_math.h"


/*****************************************************************************/
/* generate a random num from min to max inclusive on both ends */

int rand_num( int min, int max )
{
     /* prevent /0 errors and strange numbers */
     if( min > max )
     {
          fprintf(stderr, "for rand_num(%d, %d), max must be > min.\n", min, max);
          return max;
     }
     else if( min == max )
          return max;

return rand()%(max-min+1)+min;
}

/*****************************************************************************/
