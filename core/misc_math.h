/*
* Various math functions.
*
* Copyright (C) 2003,2004 David Redick
* Released under the GNU General Public License (v2)
*/

#ifndef _LWMAPGEN_MISC_MATH_H
#define _LWMAPGEN_MISC_MATH_H


/* math.h files do not define M_PI in ANSI C  */
#ifndef M_PI
#define M_PI     3.14159265358979323846264338327950288
#endif

#define deg2rad(d)     (M_PI*(d)/180)


/* a random number from min to max inclusive on both ends */
int rand_num( int min, int max );


#endif
