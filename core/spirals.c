/*
* Makes crazy spirals, reminiscent of the hypno love swirl of the seventies
*
* Copyright (C) 2003,2004 Chris Guirl
* Released under the GNU General Public License (v2)
*/

#include <math.h>
#include <stdio.h>

#include "map.h"
#include "misc_math.h"


static void get_coords(int cx, int cy, double rad, int deg, int *x, int *y)
{
     *x = cx + rad * cos(deg2rad(deg));
     *y = cy + rad * sin(deg2rad(deg));
return;
}

void spirals()
{
     int vert1[8], whitespace1[8];
     int vert2[8], whitespace2[8];
     int centerx, centery;
     int length, color, degree;
     int num_spirals = rand_num(5, 15);
     double radius;

     while( num_spirals-- )
     {
          centerx = rand_num(10, map.width - 10);
          centery = rand_num(10, map.height - 10);
          radius = 0;
          length = rand_num(360, 1080); /* (rand()%720)+360; */

printf("new spiral, starting at (%d, %d)\n", centerx, centery);

          for( degree = 0; degree < length; degree += 5 )
          {
               color = ( (degree % 270) == 0 ) ? 255 : 0;

               /* section of white space to go behind spiral */
               get_coords( centerx, centery, radius + 7, degree, &(whitespace1[0]), &(whitespace1[1]));
               get_coords( centerx, centery, radius + 7.5, degree + 5, &(whitespace1[2]), &(whitespace1[3]));
               get_coords( centerx, centery, radius - 10.5, degree + 5, &(whitespace1[4]), &(whitespace1[5]));
               get_coords( centerx, centery, radius - 11.0, degree, &(whitespace1[6]), &(whitespace1[7]));

               /* "other side" of section of white space to go behind spiral */
               get_coords( centerx, centery, radius + 7, degree + 180, &(whitespace2[0]), &(whitespace2[1]));
               get_coords( centerx, centery, radius + 7.5, degree + 5 + 180, &(whitespace2[2]), &(whitespace2[3]));
               get_coords( centerx, centery, radius - 10.5, degree + 5 + 180, &(whitespace2[4]), &(whitespace2[5]));
               get_coords( centerx, centery, radius - 11.0, degree + 180, &(whitespace2[6]), &(whitespace2[7]));

               /* alternate line points for first segment of spiral */
               if( degree == 0 )
               {
                    /* section of spiral */
                    get_coords( centerx, centery, radius, degree, &(vert1[0]), &(vert1[1]));
                    vert1[2] = vert1[0];
                    vert1[3] = vert1[1];
                    get_coords( centerx, centery, radius + 3.5, degree + 5, &(vert1[4]), &(vert1[5]));
                    get_coords( centerx, centery, radius + 0.5, degree + 5, &(vert1[6]), &(vert1[7]));

                    /* "other side" of section of spiral */
                    get_coords( centerx, centery, radius, degree + 5 + 180, &(vert2[0]), &(vert2[1]));
                    vert2[2] = vert2[0];
                    vert2[3] = vert2[1];
                    get_coords( centerx, centery, radius + 3.5, degree + 5 + 180, &(vert2[4]), &(vert2[5]));
                    get_coords( centerx, centery, radius + 0.5, degree + 5 + 180, &(vert2[6]), &(vert2[7]));
               }
               else
               {
                    /* section of spiral */
                    get_coords( centerx, centery, radius + 3.0, degree - 2, &(vert1[0]), &(vert1[1]));
                    get_coords( centerx, centery, radius + 3.5, degree + 7, &(vert1[2]), &(vert1[3]));
                    get_coords( centerx, centery, radius + 0.5, degree + 7, &(vert1[4]), &(vert1[5]));
                    get_coords( centerx, centery, radius, degree - 2, &(vert1[6]), &(vert1[7]));

                    /* "other side" of section of spiral */
                    get_coords( centerx, centery, radius + 3.0, degree - 2 + 180, &(vert2[0]), &(vert2[1]));
                    get_coords( centerx, centery, radius + 3.5, degree + 7 + 180, &(vert2[2]), &(vert2[3]));
                    get_coords( centerx, centery, radius + 0.5, degree + 7 + 180, &(vert2[4]), &(vert2[5]));
                    get_coords( centerx, centery, radius, degree - 2 + 180, &(vert2[6]), &(vert2[7]));
               }

               polygon(map.map, 4, whitespace1, 255);
               polygon(map.map, 4, whitespace2, 255);
               polygon(map.map, 4, vert1, color);
               polygon(map.map, 4, vert2, color);

               /* increment radius */
               radius += 0.5;
          }
     }

     rect(map.map, 1, 1, map.width-2, map.height-2, 255);
     rect(map.map, 0, 0, map.width-1, map.height-1, 0);
     
return;
}
