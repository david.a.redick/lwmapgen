/*
* Info on the functions to generate the map.
*
* Copyright (C) 2003,2004 David Redick
* Released under the GNU General Public License (v2)
*/

#ifndef _LWMAPGEN_FUNC_H
#define _LWMAPGEN_FUNC_H



#define LWMAPGEN_DEFT_FUNC     -1     /* default */
#define LWMAPGEN_RAND_FUNC     -1
#define LWMAPGEN_MIN_FUNC       0
#define LWMAPGEN_MAX_FUNC      15


typedef struct
{
     char *name;
     char *desc;
     void (*func)();
} func_t;

extern func_t func[LWMAPGEN_MAX_FUNC];


#endif
