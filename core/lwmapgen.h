/*
* The basic wrapper/interface to the random map generator.
*
* Read comment in lwmapgen.c for usage.
*
* Copyright (C) 2003,2004, David Redick
* Released under the GNU General Public License (v2)
*/

#ifndef _LWMAPGEN_H
#define _LWMAPGEN_H


#include "allegro.h"
#include "map.h"
#include "func.h"


#define LWMAPGEN_VERSION     "0.1.11"


BITMAP * lwmapgen( int size, int grid_size, int func_id );

#endif
