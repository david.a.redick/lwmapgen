/*
* Makes crazy spirals, reminiscent of the hypno love swirl of the seventies
*
* Copyright (C) 2003,2004 Chris Guirl, David Redick
* Released under the GNU General Public License (v2)
*/

#include <math.h>
#include <stdio.h>

#include "map.h"
#include "misc.h"
#include "misc_math."


static int global_var;


static void chkpixel(BITMAP *bmp, int x, int y, int col)
{
     if ( getpixel(bmp, x, y) == col )
          global_var = 1;
}


static int used(BITMAP *bmp, int x1, int y1, int x2, int y2, int col)
{
     global_var = 0;

     /*line(bmp, x1, y1, x2, y2, 127);*/
     do_line(bmp, x1, y1, x2, y2, col, chkpixel);

return global_var;
}


static int chkcircle(BITMAP *bmp, int x, int y, int rad, int col)
{
     int i;
     global_var = 0;

     /* check all circles from radius 0 to radius rad */
     for( i = 0; i <= rad; i++ )
     {
          /*circle(bmp, x, y, rad, 127);*/
          do_circle(bmp, x, y, i, col, chkpixel);
          if( global_var ) break;
     }

return global_var;
}


#define radians(d)     (M_PI * (d) / 180.0)


#define degrees(r)     ((r) * 180.0 / M_PI)


static void get_coords(int cx, int cy, double rad, double deg, int *x, int *y)
{
     *x = cx + rad * cos(radians(deg));
     *y = cy + rad * sin(radians(deg));
return;
}


static int off_map(int x1, int y1, int x2, int y2, int w)
{
     /* check to see if we're in the outer w pixels of the map */
     return ( (x1 < w) || 
              (x2 < w) ||
              (y1 < w) || 
              (y2 < w) || 
              (x1 > map.width - w) || 
              (x2 > map.width - w) ||
              (y1 > map.height - w) || 
              (y2 > map.height - w) );
#if 0
     /* if we're almost off the map, cleverly advance along the arc to the place where the wall ends again*/
     xnext = centerx - (checkx - centerx);
     ynext = centery - (checky - centery);
     degree = degrees(acos((checkx - centerx) / rad));
     /* x = cx + rad * cos(radians(deg)); eqn we're starting from...
     y = cy + rad * sin(radians(deg)); */
#endif
}

/*
* we could keep looping for a very long time 
* so we have to put a cap on it.
*/
#define MAX_LOOP     500

void spirals()
{
     int loop;  /* tally of the number of times we have looped */
     int n;     /* tally of spirals drawn */
     int dir;
     int centerx, centery;
     int x1a, y1a, x2a, y2a, checkx, checky;
     int x1b, y1b, x2b, y2b, checkendx, checkendy;
     int length, color, degree;
     int num_spirals = rand_num(5, 25);
     double radius;

     loop = 0;
     n = 0;
     while( n < num_spirals && loop < MAX_LOOP )
     {
          loop++;
          centerx = rand_num(10, map.width - 10);
          centery = rand_num(10, map.height - 10);
          radius = 0;
          length = rand_num(360, 1800); /* (rand()%1440)+360; */

          if( (n == 0) || ( chkcircle(map.map, centerx, centery, 10, 0) == 0 ) )
          {
printf("new spiral, starting at (%d, %d)\n", centerx, centery);
               n++;

               for( dir = 0; dir <= 1; dir++ )
               {
                    for( degree = 0 + (dir * 180); degree < length + (dir * 180); degree += 5 )
                    {
                         /* point from which to begin checking for collision */
                         get_coords( centerx, centery, radius + 0.25, degree + 2.5, &checkx, &checky);
                         get_coords( centerx, centery, radius + 0.5, degree + 5, &checkendx, &checkendy);

                         if(  radius >= 4
                         && ( off_map(checkx, checky, checkendx, checkendy, 4)
                              || used(map.map, checkx, checky, checkendx, checkendy, 0) == 1 ) )
                         {
printf("*** encountered another spiral, stopping...\n");
                              get_coords( centerx, centery, radius - 2.0, degree, &x1a, &y1a);
                              get_coords( centerx, centery, radius + 2.0, degree, &x1b, &y1b);

                              x2a = x1a;
                              x2b = x1b;
                              y2a = y1a;
                              y2b = y1b;
                              break;
                         }

                         color = (degree % (270 + (dir * 180)) == 0) ? 255 : 0;

                         /* alternate line points for first segment of spiral */
                         if( degree == (0 + (dir * 180)) )
                         {
                              /* point from which lines a & b start */
                              get_coords( centerx, centery, radius, degree, &x1a, &y1a);
                              x1b = x1a;
                              y1b = y1a;

                              /* point at which lines a & b end */
                              get_coords( centerx, centery, radius - 1.5, degree + 5, &x2a, &y2a);
                              get_coords( centerx, centery, radius + 1.5, degree + 5, &x2b, &y2b);
                         }
                         else
                         {
                              /* point from which lines a & b begin */
                              get_coords( centerx, centery, radius - 2.0, degree, &x1a, &y1a);
                              get_coords( centerx, centery, radius + 2.0, degree, &x1b, &y1b);

                              /* point at which lines a & b end */
                              get_coords( centerx, centery, radius - 1.5, degree + 5, &x2a, &y2a);
                              get_coords( centerx, centery, radius + 2.5, degree + 5, &x2b, &y2b);
                         }

                         line(map.map, x1a, y1a, x2a, y2a, color);
                         line(map.map, x1b, y1b, x2b, y2b, color);

                         if( ( degree % (270 + (dir * 180)) >= (265 + (dir * 180)) ) || ( degree % (270 + (dir * 180)) < 5 ) )
                         {
                              /* cap the end/beginning of the spiral segments, if at the end, paint */
                              get_coords(centerx, centery, radius, degree+5, &checkx, &checky);
                              line(map.map, x2a, y2a, checkx, checky, 0);
                              line(map.map, x2b, y2b, checkx, checky, 0);
                              if( degree % (270 + (dir * 180)) < 5 )
                              {
                                   get_coords( centerx, centery, radius-0.5, degree - 5, &checkx, &checky);
                                   floodfill(map.map, checkx, checky, 0);
                              }
                         }

                         /* increment radius */
                         radius += 0.5;

                    } /* for(deg..) */


                    /* cap end of spiral */
                    get_coords( centerx, centery, radius+0.5, degree, &checkx, &checky);
                    line(map.map, x2a, y2a, checkx, checky, 0);
                    line(map.map, x2b, y2b, checkx, checky, 0);

                    /* paint the last segment in */
                    get_coords( centerx, centery, radius-1, degree - 10, &checkx, &checky);
                    floodfill(map.map, checkx, checky, 0);

                    /* cut out the center of the spiral */
                    rectfill(map.map, centerx-1, centery-1, centerx, centery, 255);

                    /* reset radius to go around the other way */
                    radius = 0;

               } /* for(dir..) */

          } /* if(starting new spiral) */

     } /* for(num_spirals..) */

return;
}
