/*
* The shell interface to the Liquid War Random Map Generator.
*
* Copyright (C) 2003,2004 David Redick, Christian Mauduit
* Released under the GNU General Public License (v2)
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "lwmapgen.h"


#define DEFT_FILENAME     "lwmap.bmp"     /* default filename */
char *filename = NULL;
int size       = LWMAPGEN_DEFT_MAP_SIZE;
int grid_size  = LWMAPGEN_DEFT_GRID_SIZE;
int func_id    = LWMAPGEN_DEFT_FUNC;


void do_args(int argc, char **argv);
void print_help();
void print_version();


int main( int argc, char **argv )
{
     PALETTE pal;
     int i;


     /* init allegro and init palette */
     install_allegro(SYSTEM_NONE, &errno, atexit);
     set_color_depth(8);
     set_color_conversion(COLORCONV_REDUCE_TO_256);
     for( i = 0; i < 256; i++ )
     {
          /* divided by 4 because the colour value ranges from 0-63 */
          pal[i].r = pal[i].g = pal[i].b = i/4;
     }


     srand(time(NULL));

     do_args(argc, argv);

     if( filename == NULL )
     {
          /* +1 for '\0' char */
          filename = malloc(strlen(DEFT_FILENAME)+1);
          if( filename == NULL )
          {
               fprintf(stderr, "error: can't malloc space for map name.\n");
               exit(EXIT_FAILURE);
          }
          strcpy(filename, DEFT_FILENAME);
     }

     lwmapgen(size, grid_size, func_id);

     save_bitmap(filename, map.map, pal);


return EXIT_SUCCESS;
}
END_OF_MAIN();

/*****************************************************************************/

void do_args( int argc, char **argv )
{
     int i;

     for( i = 1; i < argc; i++ )
     {
          if( strcmp(argv[i], "-o") == 0 || strcmp(argv[i], "--out") == 0 )
          {
               if( filename != NULL )
                    free(filename);

               /* +1 for '\0' char */
               filename = malloc(strlen(argv[++i])+1);
               if( filename == NULL )
               {
                    fprintf(stderr, "error: can't malloc space for map name.\n");
                    exit(EXIT_FAILURE);
               }
               strcpy(filename, argv[i]);
          }
          else if( strcmp(argv[i], "-s") == 0 || strcmp(argv[i], "--size") == 0 )
          {
               size = atoi(argv[++i]);
               if( size == LWMAPGEN_RAND_MAP_SIZE )
                    /* do nothing */;
               else if( size < LWMAPGEN_MIN_MAP_SIZE )
               {
                    fprintf(stderr, "map size too small using: %d\n",
                         LWMAPGEN_MIN_MAP_SIZE);
                    size = LWMAPGEN_MIN_MAP_SIZE;
               }
               else if( size >= LWMAPGEN_MAX_MAP_SIZE )
               {
                    fprintf(stderr, "map size too large using: %d\n",
                         LWMAPGEN_MAX_MAP_SIZE-1);
                    size = LWMAPGEN_MAX_MAP_SIZE-1;
               }
          }
          else if( strcmp(argv[i], "-g") == 0 || strcmp(argv[i], "--grid") == 0 )
          {
               grid_size = atoi(argv[++i]);
               if( grid_size == LWMAPGEN_RAND_GRID_SIZE )
                    /* do nothing */;
               else if( grid_size < LWMAPGEN_MIN_GRID_SIZE )
               {
                    fprintf(stderr, "map grid too small using: %d\n",
                         LWMAPGEN_MIN_GRID_SIZE);
                    grid_size = LWMAPGEN_MIN_GRID_SIZE;
               }
               else if( grid_size >= LWMAPGEN_MAX_GRID_SIZE )
               {
                    fprintf(stderr, "map grid too large using: %d\n",
                         LWMAPGEN_MAX_GRID_SIZE-1);
                    grid_size = LWMAPGEN_MAX_GRID_SIZE-1;
               }
          }
          else if( strcmp(argv[i], "-f") == 0 || strcmp(argv[i], "--function") == 0 )
          {
               int f;
               i++;
               /* find the matching function name */
               for( f = 0; f < LWMAPGEN_MAX_FUNC; f++ )
               {
                    if( strcmp(argv[i], func[f].name) == 0 )
                         break;
               }
               if( f >= LWMAPGEN_MAX_FUNC )
               {
                    fprintf(stderr, "error: can't find function: %s\n", argv[i]);
                    if( func_id == LWMAPGEN_RAND_FUNC )
                         fprintf(stderr, "using default: random\n");
                    else
                         fprintf(stderr, "using default: %s\n", func[func_id].name);
               }
               else
                    func_id = f;
          }
          else if( strcmp(argv[i], "-l") == 0 || strcmp(argv[i], "--list") == 0 )
          {
               /* list all the functions */
               int f;
               printf("Random map generating functions:\n");
               printf("-1)  %s\t%s\n", "random", "Picks a random function.");
               for( f = 0; f < LWMAPGEN_MAX_FUNC; f++ )
                    printf("%2d)  %s\t%s\n", f, func[f].name, func[f].desc);

               exit(EXIT_SUCCESS);
          }
          else if( strcmp(argv[i], "--help") == 0 )
          {
               print_version();
               print_help();
               exit(EXIT_SUCCESS);
          }
          else if( strcmp(argv[i], "--version") == 0 )
          {
               print_version();
               exit(EXIT_SUCCESS);
          }
          else
          {
               fprintf(stderr, "error: unknown arg: %s\n", argv[i]);
               fprintf(stderr, "ignoring...\n");
          }
     }

return;
}

/*****************************************************************************/

void print_help()
{
     int i;

     printf("\n");
     printf("-o, --out       <filename>        Save bitmap to <filename>.    [%s]\n",
          DEFT_FILENAME);
     printf("-s, --size      <%d-%d>             Bitmap size.                  [%d]\n",
          0, LWMAPGEN_MAX_MAP_SIZE-1, LWMAPGEN_DEFT_MAP_SIZE);
     printf("-g, --grid      <%d-%d>             Map Grid Size                 [%d]\n",
          0, LWMAPGEN_MAX_GRID_SIZE-1, LWMAPGEN_DEFT_GRID_SIZE);
     printf("\n");
     /* TODO: I'm really just assuming that is random... need to check func_id */
     printf("-f, --function  <function_name>   Which function to use.        [random]\n");
     printf("-l, --list                        List all functions.\n");
     printf("\n");
     printf("    --help                        Print this help.\n");
     printf("    --version                     Print Version.\n");

     printf("\nMap Sizes (WxH):\n");
     printf("\t(-1) random\n");
     for( i = LWMAPGEN_MIN_MAP_SIZE; i < LWMAPGEN_MAX_MAP_SIZE; i++ )
          printf("\t(%d)%dx%d\n", i,  map_size[i][0], map_size[i][1]);
     printf("\n");

     printf("Grid Sizes (RxC):\n");
     printf("\t(-1) random\n");
     for( i = LWMAPGEN_MIN_GRID_SIZE; i < LWMAPGEN_MAX_GRID_SIZE; i++ )
     {
          printf("\t(%d)%dx%d\n", i, map_grid_size[i][0], map_grid_size[i][1]);
     }
     printf("\n");

return;
}

/*****************************************************************************/

void print_version()
{
     printf(
     "Liquid War Random Map Generator, version %s\n"
     "Copyright (C) 2003,2004, David Redick, Chris Guirl, Christian Mauduit.\n"
     "Released under the GNU General Public License (v2).\n",
     LWMAPGEN_VERSION
     );

return;
}

/*****************************************************************************/
