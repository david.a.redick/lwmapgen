# Copyright (C) 2003,2004 David Redick
# Released under the GNU General Public License (v2)

include Makefile.in

EXE=$(LWMAPGEN_EXE)
LIB=$(LWMAPGEN_LIB)
CFLAGS=$(EXE_CFLAGS)
LDFLAGS=$(EXE_LDFLAGS)


all: $(EXE)

$(EXE): $(LIB) main.c
	$(CC) $(CFLAGS) main.c $(LDFLAGS) -o $(EXE)
	@strip $(EXE)

$(LIB): ./core/*.[ch]
	@make -C core

check: test
test: all
	@./test.sh &> test.log && ./view.sh

clean: cleanmaps
	rm -rf $(EXE) $(LIB) *~ *.bmp
	rm -rf test.log
	@make -C core clean

cleanmap: cleanmaps
cleanmaps:
	rm -f ./test_maps/*.bmp
