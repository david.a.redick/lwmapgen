#!/bin/sh
# This script select a image viewer and
# then views all the test maps

# Copyright (C) 2004 David Redick, Chris Guirl
# Released under the GNU General Public License (v2).


mapdir=./test_maps
viewer_list="qiv ee xv"


# find a viewer
for v in $viewer_list; do
	viewer=`which $v 2> /dev/null`
	if [ $viewer ]; then
		break
	fi
done

# check if there is a viewer
if [ -z "$viewer" ]; then
	echo "Can't find image viewer."
	echo "Viewers: $viewer_list"
	echo "If your image viewer isn't listed then add it to \$viewer_list"
	exit 1
fi


# if we're logged in via ssh this isn't set
# well its silly but its a good thing(R)(TM)(C) to test for
if [ -z "$DISPLAY" ]; then
	echo "\$DISPLAY is not set."
	exit 1
fi


# the view loop...
cd $mapdir
for file in *.bmp; do
	echo "*** viewing: $file ***"
	$viewer $file
done
