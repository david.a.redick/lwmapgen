#!/bin/sh
# This is just a script to automate testing
#
# If you want to test just one function then do:
# 	testfunc=bubbles ./test.sh
#
# Copyright (C) 2004 David Redick, Chris Guirl
# Released under the GNU General Public License (v2)


lwmapgen="./liquidwar-mapgen"
mapsize="-1 0 1 2 3 4 5"
gridsize="-1 0 1 2 3 4 5 6 7"
mapdir=./test_maps

# check if we have the binary
if [ ! -x $lwmapgen ]; then
	echo "error: cannot find $lwmapgen."
	echo "run make to compile"
	exit 1
fi


# make sure we have the current dir in our ld path
# some distros are fucking stupid and don't have this
# which means we can't test run our app
export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:."


# clean up the old maps
rm -f $mapdir/*.bmp


if [ -z $testfunc ]; then
	testfunc="big_quad boxes bubbles burst circles circuit hole lines \
		maze random rand_box rand_poly rand_poly_cut spirals street worms"
fi

echo "Testing: $testfunc"



# generate all our test maps
for func in $testfunc; do
	for s in $mapsize; do
		for g in $gridsize; do
			echo "*** testing: $func, $s, $g ***"
			$lwmapgen -f $func -s $s -g $g -o $mapdir/$func.$s.$g.bmp
			# &> /dev/null
			sleep 2
		done
	done
done
